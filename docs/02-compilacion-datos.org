#+STARTUP: indent
#+STARTUP: overview

:REVEAL_PROPERTIES:
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_THEME: simple
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+OPTIONS: timestamp:nil toc:1 num:nil author:nil date:nil
:END:  

#+TITLE: 2-Compilación de datos
#+SUBTITLE: Visualización de la Información
#+AUTHOR: Julián Pérez
#+DATE: 18-02-22
#+LANGUAGE: es
#+EMAIL:jperez@esdmadrid.es
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+REVEAL_HIGHLIGHT_CSS: https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.4.0/styles/base16/espresso.min.css
#+REVEAL_EXTRA_CSS: ../assets/css/modifications.css
#+REVEAL_EXTRA_CSS: ../assets/fonts/webfont-iosevka-11.3.0/iosevka.css
#+REVEAL_PLUGINS: (highlight CopyCode)
#+REVEAL_TITLE_SLIDE: <h1 class="r-fit-text" >%t</h1><br><br><h3 class="subtitle">%s</h3><br><h4>%a</h4><h4>%d</h4><p>Escuela Superior de Diseño de Madrid</p>
#+OPTIONS: toc:nil

* Índice
  - [[Recolección de datos][Recolección de datos]]
  - [[La vida real][La vida real]]
  - [[Tabula][Tabula]]
  - [[Google spreadsheets][Google spreadsheets]]
  - [[La web como fuente de datos][La web como fuente de datos]]
* Resumen
- Proceso para la petición de datos y qué organismos gestionan el acceso a la información
- Conoceremos diferentes técnicas de compilación de datos como Tabula, importHTML, importXML, etc.
- Otras técnicas como hacer búsqueda avanzada desde buscadores en internet.
* Recolección de datos
** Acceso a la información
- *Derecho* de las sociedades democráticas
- El *estado* salvaguarda el ejercicio de este derecho 
- Garantizarlo de manera *digital y pública* a través de Internet
- *Estándares abiertos, formatos interoperables y reutilizables*
** Organismos
- Públicos: [[https://datos.gob.es][datos.gob.es]]
- No Gubernamentales:
  - [[https://okfn.org][Open Kwnoledge Foundation]]
  - [[https://www.access-info.org/][Access Info Europe]]
  - [[https://theodi.org/][ODI]]
  - [[https://www.proacceso.org/][ProAcceso]]
- No toda la información es accesible a los periodistas ni al público en general.
** Ley de Transparencia
- Ley de Transparencia y Derecho de Acceso a la Información Pública
- Publicada en el BOE 10 Diciembre *2013*
- *Obliga* a que las administraciones publiquen información relevante y actual sin que la ciudadanía se la solicite a través de Internet
- ¿Cuáles son las administraciones del estado?
** Peticiones de información
- Derecho a *reclamar* acceso a la información pública
- Puede que la petición sea denegada
- Si afecta a datos personales especialmente protegidos, persona afectada consentimiento.
- No es obligatorio exponer los motivos de la solicitud de información.
- Administración tendrá un mes para resolver la consulta
** Ética de recolección de datos
#+BEGIN_NOTES
- ... que hacen las organicaciones de los datos
- ...
- ... Por ej.: sólo recoger datos de las áreas menos contaminadas de una ciudad
#+END_NOTES
- Aplicación de normas éticas en la recolección, gestión y uso
- Organizaciones establecen pautas específicas para cada area de conocimiento
- No sólo se aplica la ética de uso de datos cuando haya datos personales involucrados
#+REVEAL: split:t
#+BEGIN_NOTES
- ... Esto es algo complementario, pero se escapan muchos otros aspectos por cubrir
- ...
- ...
#+END_NOTES
- No vale con cumplir las directivas y leyes de protección de datos.
- La ética debe de estar en todas las etapas del ciclo de vida y gestión del uso de datos.
- Ethicas Foundation
* La vida real
** En la práctica
- Hay muchos casos en los que las administraciones no llegan a ofrecer los datos de una forma estructurada
- Se hacen las solicitudes de información y no se suministra
- Datos en PDF que contiene una imagen, un formato pensado para otra cosa
- No facilita recolectar esa información
** Herramientas recolección datos
- GUI (/Graphic User Interface/): Tabula, Google SpreadSheets
- CLI (/Command Line Interface/): =wget= y =curl=
- Beautiful Soup (Pyhton)
- etc.
* Tabula
- [[https://tabula.technology/][Tabula]] es una herramienta sencilla que nos sirve para extraer tablas de datos dentro de PDFs
- Es libre, de código abierto y fue creada por periodistas para periodistas
- Actualmente es un software que se mantiene por la comunidad de desarrollo de forma voluntaria
#+REVEAL: split:t
- Ocasionalmente ha tenido financiación para funciones específicas, pero nunca ha sido una iniciativa comercial
- Soportada por medios especializados y entidades con una visión amplia de los datos abiertos
** IMPORTANTE!
Sólo funciona con PDFs basados en texto, no sirvirá para PDFs de escaneo de imágenes (para esto existen herramientas OCR, reconocimiento óptico de caracteres).
** Práctica con Tabula
 - Tabula, como sabemos, no aparece como un programa sino que para abrirlo debemos ir a la carpeta donde lo tengamos y ejecutarlo.
 - Al abrirse conecta con la máquina virtual de Java y arranca el programa como aplicación web en una pestaña del navegador si lo tenemos abierto o abre el navegador si no lo tenemos.
 - Si no se abre, comprobad en un navegador esta dirección 127.0.0.1 en el puerto 8080, es decir, http://127.0.0.1:8080
 #+REVEAL: split:t
 - Descargamos este [[https://www.xunta.gal/dog/Publicados/2021/20211027/AnuncioG0596-300921-0001_es.pdf][PDF]] para trabajar sobre él.
 - En Tabula, pinchamos en el botón "Browse" para cargar el PDF que acabamos de descargar.
 - Después pulsamos el botón "Import".
 #+REVEAL: split:t
 - Nos previsualizará el PDF que acabamos de importar
 - Para empezar vamos a probar con la opción para que detecte de forma automática las tablas. Pinchamos botón "Autodetect Tables"
 - Revisamos las tablas que ha detectado. Es posible que se haya dejado alguna por el camino o que la selección automática no encuadre bien la tabla.
 #+REVEAL: split:t
 - Si esto ocurre, pinchamos y arrastramos los tiradores de la tabla para marcarla de forma manual.
 - Cuando tengamos todo lo que queremos seleccionado pinchamos en "Preview & Export Extracted Data"
 - Si todo está bien exportamos en formato CSV y guardamos el archivo.
** Abrir archivos CSV
 - Los archivos CSV son archivos de datos separados por comas, "Comma Separated Values".
 - Hemos aprendido a ver CSV en la terminal? [[https://csvkit.readthedocs.io/en/latest/tutorial/1_getting_started.html][csvkit]]
 - También podemos usar un programa gráfico como Excel --si lo tenéis-- o Google Spreadsheets o [[https://www.libreoffice.org/][LibreOffice]], que es gratuito.
 #+REVEAL: split:t
 - En estos programas nos preguntará como queremos importarlo.
 - Es importante que en conjunto de caracteres usemos "Unicode UTF-8" y en opciones de separador: separado por "coma".

* Google spreadsheets 
- /Google Spreadsheets/ cuenta con dos funciones muy interesantes para recopilar datos.
- =importhtml= sirve para importar datos de tablas y listas.
- =importxml= sirve para importar datos de cualquier parte de la página.
** IMPORTHTML 
- Con =IMPORTHTML= podemos importar dos tipos de elementos, tabulados o =table= y listados =list= (=ul=, =ol= y =dl=)
  - =ul=, que corresponde a /unordered list/ o lista desordenada, la típica lista donde cada elemento aparece con un punto o un guión.
  - =ol=, que corresponde a /ordered list/ o lista ordenada, donde los elementos del listado aparecerán ordenados, bien numérica o alfabéticamente, por ejemplo.
  - =dl=, corresponde con /description list/, listas de descripciones.
  - =list= no existe en HTML, es una forma de denominar desde la función todos los elementos de listado de HTML.
#+REVEAL: split:t
- Se construye la expresión con la /URL/ entrecomillada, separado por punto y coma y entrecomillado el elemento del que queremos sacar la información, bien una lista =list= o una tabla =table=, seguido de =n=, número de elemento en la página de su mismo tipo, separado por otro punto y coma:
#+BEGIN_SRC bash
=IMPORTHTML("URL";"list|table";n)
#+END_SRC
- Si esos datos están bien estructurados en origen los podremos tener bien estructurados en nuestra hoja de datos.
** Práctica con IMPORTHTML
- Iniciamos sesión con la cuenta de gmail y vamos a [[https://drive.google.com/drive][drive]] y creamos una hoja o directamente a [[https://docs.google.com/spreadsheets][google spreadsheets]].
- Para esta práctica queremos obtener la tabla [[https://es.wikipedia.org/wiki/Demograf%C3%ADa_de_Espa%C3%B1a#Diez_principales_provincias_por_poblaci%C3%B3n]["Diez principales provincias por población"]] de este [[https://es.wikipedia.org/wiki/Demograf%C3%ADa_de_Espa%C3%B1a][artículo en wikipedia]].
- Copiamos la url de este artículo y volvemos a la hoja de cálculo.
#+REVEAL: split:t
- En la primera celda de nuestra hoja introducimos esta estructura: ==IMPORTHTML("url";"elemento";n)=
- Para utilizar funciones acordaros de iniciarlas en la celda con el símbolo ===
- No hay una forma mágica de saber el número de elemento pero la ejecución de resultados es rápida por lo que vamos probando 1, 2, 3, hasta que lo conseguimos.
#+REVEAL: split:t
- Si lo hemos hecho bien deberían importarse los campos de la tabla que buscamos.
- Si no, nos dará un mensaje de error tipo =#error= o =#n/a= (de not available).
- Si ocurre esto, revisamos el mensaje colocando el cursor encima de la celda y nos dará una pista de lo que estamos haciendo mal.
#+REVEAL: split:t
- Probamos lo mismo para importar la _lista_ "Proporción hombres/mujer en España (2019)"
- En la función tenemos que cambiar =table= por =list= y cambiar el número hasta que demos con ella.
#+REVEAL: split:t
- Probamos esta función junto con la función [[https://developers.google.com/chart/interactive/docs/querylanguage][=query()=]] para filtrar y ordenar resultados
- Probaremos con esta [[http://acb.com/estadisticas-individuales/valoracion/temporada_id/2020/fase_id/107][tabla de la ACB]].
#+BEGIN_SRC bash
=QUERY(IMPORTHTML("http://acb.com/estadisticas-individuales/valoracion/temporada_id/2020/fase_id/107";"table";1);"SELECT * WHERE Col1 < 10";2)
#+END_SRC
** IMPORTXML 
- La función ==IMPORTXML= nos servirá para obtener más elementos de una web a diferencia de =IMPORTHTML=.
- Podremos obtener bloques de texto, listas, URLS a otras webs, o la fuente de imágenes, audios, videos, etc.
- Es una herramienta que nos sirve para casos sencillos de recopilación de datos de una manera gráfica.
#+REVEAL: split:t
- Tiene un poco más de complejidad que =IMPORTHTML= porque para encontrar los elementos que queremos de la web lo hacemos a través de peticiones o /querys/ en lenguaje [[https://www.w3.org/TR/xpath][XPATH]].
- El esquema es:
#+BEGIN_SRC bash
 =IMPORTXML("url";"consulta-xpath")
#+END_SRC
** XPATH
*** XPATH
 - [[https://www.w3.org/TR/xpath][XPath]] es el acrónimo de =XML Path= o ruta XML, donde /XML/ es /eXtensible Markup Language/ o lenguaje de marcas extensible.
 - Se trata de identificar los nodos de un archivo /XML/. La web son archivos HTML pero al ser renderizados por el navegador crea un DOM (/Document Object Model/, modelo de objetos de documento) como un árbol de objetos que puede ser leído por /XPATH/.
 - Nos permite navegar por los elementos y atributos de un documento /XML/.
 - /HTML/ es una forma de /XML/.
*** HTML y XML
 - La sintaxis de /XML/ es similar a la de /HTML/
 - Pero los elementos son distintos y los propósitos también son diferentes.
 - /HTML/ nos permite _dar formato_ a diversos contenidos de una página y /XML/ nos ayuda a _organizar_ los contenidos
 - Además /HTML/ tiene elementos predefinidos (=<h1></h1>=) mientras que XML nos permite crear elementos nuevos (=<libro></libro>=).
*** Archivo de datos
 - Por esta facilidad y capacidad de creación /XML/ es uno de los formatos de datos.
 - Sirve para componer documentos operables entre aplicaciones diferentes.
 - Funciona como sistema de "bases de datos".
 - /XML/ es un documento "bien formado", se tiene que respetar una estructura jerárquica con las etiquetas que delimitan sus elementos.
 - Las etiquetas deben de estar correctamente anidadas. /HTML/ es más laxo en este sentido
 #+REVEAL: split:t
 - Sólo permiten un elemento raiz o root del que todos los demás sean parte
 - Es sensible a mayúsculas y minúsculas (Case sensitive)
 - Se recomienda un recorrido por esta guía de [[https://www.w3schools.com/xml/xpath_intro.asp][w3c]] 
*** Práctica con IMPORTXML
 - Abrimos una nueva hoja de cálculo y colocamos la siguiente función:
#+BEGIN_SRC bash
   =IMPORTXML("URL","XPATH QUERY")=
#+END_SRC
 - Vamos a probar con un ejemplo sencillo. En "URL" pegamos la siguiente manteniendo las comillas dobles: https://www.w3schools.com/xml/books.xml 
 - Ahora tenemos que construir la consulta /XPATH/. Para ello repasaremos varias expresiones para seleccionar elementos que podemos ir probando con el /XML/ que se indicaba antes.
#+REVEAL: split:t
 - Prueba los diferentes ejemplos que vienen a continuación de expresiones /XPATH/ en el campo "XPATH QUERY" dentro del /IMPORTXML/ y compara los resultados con la estrucutra del archivo /books.xml/
 - En la sesión probaremos estas expresiones en un documento /HTML/
*** Expresiones XPath
 - Veremos algunas expresiones XPATH pero si quieres profundizar [[https://devhints.io/xpath][aquí]] puedes ver más expresiones y ejemplos
 - =/= Selecciona desde el nodo raiz.
   - Ejemplo: =/bookstore= Selecciona el elemento raiz =bookstore=
   - Ejemplo: =bookstore/book= Selecciona todos los elementos =book= que son parte de =bookstore=
 - =//= Selecciona nodos en el documento desde el nodo actual que coincidan con la selección, no importa dónde estén
   - Ejemplo: =//book= Selecciona todos los elementos book no importan dónde estén
   - Ejemplo: =bookstore//book= Selecciona todos los elementos =book= que son descendentes del elemento =bookstore=, no importa dónde estén dentro del elemento =bookstore=.
#+REVEAL: split:t
 - =.=, selecciona el nodo actual
 - =..=, selecciona el nodo padre del nodo actual.
 - =@= Selecciona atributos
   - Ejemplo: =//@lang= Selecciona todos los atributos que se llaman =lang=
 - =[]= Se usan para seleccionar un nodo específico o un nodo que contiene un valor específico
   - Ejemplo: =/bookstore/book[1] Selecciona el primer book que es hijo del elemento bookstore
#+REVEAL: split:t
   - Otro ejemplo, si queremos obtener el listado de todos los atributos =href= que contiene el elemento =a= que
 corresponde a los enlaces, de la /URL/, de una página web, haremos:
 #+BEGIN_SRC bash
   =IMPORTXML("URL";"//a/@href")
 #+END_SRC
*** Práctica con XPath
- En [[http://www.whitebeam.org/library/guide/TechNotes/xpathtestbed.rhtm][esta web]] puedes probar con las expresiones vistas hasta ahora con /XPath/
*** Elementos que comienzan con...

 La potencia de /Xpath/ es /infinita/ y podemos hacer extracciones de datos muy concretas, como por ejemplo seleccionar solo los elementos que comiencen con una clase específica, como =[starts-with= y luego especificar la clase con el atributo =@= donde =class= es el valor del atributo =(@class, 'clase')=
 Podríamos elegir sólo los enlaces que tienen una determinada clase, lo que haríamos también con /XPath/ de esta manera:

 #+BEGIN_SRC bash
 =IMPORTXML("URL";"//a[@class='clase']")
 #+END_SRC
#+REVEAL: split:t
  Si queremos sacar todos los enlaces de una /URL/, después de inspeccionar la página, comprobamos que los enlaces se encuentran en un =div= que tiene la clase =clase=. Construimos esta fórmula de =IMPORTXML=

 #+BEGIN_SRC bash
 =IMPORTXML("URL"; "//div[starts-with(@class,'clase')]")
 #+END_SRC
#+REVEAL: split:t
 Si quisiéramos los enlaces, añadiríamos al final =//@href=, ya que el enlace se encuentra en el atributo de =a=, =href=

 #+BEGIN_SRC bash
 =IMPORTXML("URL"; "//div[starts-with(@class,'clase')]//@href")
 #+END_SRC

 Puede ser que la página no traiga los enlaces absolutos sino que sean relativos, por lo que podemos concatenarlos con la función =CONCATENATE=:

 #+BEGIN_SRC bash
 =CONCATENATE("URL",celda-resultados)
 #+END_SRC

 Y luego estiramos esta función al resto de las celdas que lo requieran.

*** Expresiones XPath II
 - =[last()]= Selecciona el último elemento
   - Ejemplo: =/bookstore/book[last()]= Selecciona el último elemento book que es hijo del elemento bookstore
 - =[position()<>=número]= Selecciona el elemento o los elementos que se indiquen según el número y operador
   - Ejemplo: =/bookstore/book[position()<3]= Selecciona los dos primeros elementos book que son hijos del elemento bookstore
 - =[@]= Selecciona todos los elementos que tienen el atributo que se indique
   - Ejemplo: =//title[@lang]= Selecciona todos los elementos title que tienen el atributo lang
#+REVEAL: split:t
 - =*= Selecciona todos los elementos
   - Ejemplo: =/bookstore/*= Selecciona todos los elementos hijos del elemento bookstore
   - Ejemplo: =//*= Selecciona todos los elementos del documento
 - =@*= Selecciona todos los elementos que tienen al menos un atributo
   - Ejemplo: =//title[@*]= Selecciona todos los elementos title que tienen al menos un atributo
*** Algunos ejemplos XPath útiles:
 - =//=, descarga todos los elementos de html que empiecen con =<=
 - =//a=, descarga todos los contenidos del elemento =a=, los enlaces, de la URL que decidamos.
 - =//a/@href=, descarga todos los contenidos del atributo =href= del elemento =a=, que corresponden con la URL del enlace.
 - =//input[@type='text']/..=, descarga todos los elementos padre de los elementos de texto =input=
 - =count(//p)=, cuenta el número de elementos que le digamos, en este caso párrafos =p=
 - =//a[contains(@href, 'protesta')]/@href=, encuentra todos los enlaces que contienen la palabra =protesta=
 - =//div[not(@class='left')]=, encuentra todos los =div= cuyas clases no sean =left=
 - =//img/@alt=, muestra todos los textos de los atributos =alt= de las imágenes =img=
*** Práctica sesión online                                         :noexport:
 - Resolvemos dudas sobre las prácticas offline.
 - Repasamos el uso de inspector de elementos.
 - Descargamos ejemplo de [[https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov/documentos/Actualizacion_325_COVID-19.pdf][PDF]] COVID y lo probamos con Tabula. 
 - Probar código =js= en consola de navegador para detectar tablas: =i=1; [].forEach.call(document.getElementsByTagName("table"), function(x){console.log(i++);});=
 - Probar función =QUERY()= en los resultados de =IMPORTHTML=
 - Probar =IMPORTXML= con una página =HTML=
 - Recurso: [[http://www.whitebeam.org/library/guide/TechNotes/xpathtestbed.rhtm][XPath Expression Testbed]] Esta web nos permite evaluar nuestras expresiones sobre un archivo que carguemos
* La web como fuente de datos
** Inspección de la página
- El inspector de la página es una herramienta que viene incorporada tanto en el navegador firefox como chrome.
- Viene de la antigua extensión /firebug/ que servía para inspeccionar elementos de la página.
- Nos permite examinar y modificar el html y css de una página.
#+REVEAL: split:t
- También podremos aplicar scripts sobre la página desde la consola. nos será útil para identificar elementos de la web rápidamente.
- Para acceder tanto en firefox como chrome podemos llegar de varias maneras:
  1) en windows: =crtl+shift+i=
  2) en mac: =cmd+shift+i=
  3) haz click derecho en un elemento de la página web y selecciona "inspeccionar elemento" o "inspeccionar"

** Navegación
*** Operadores avanzados de búsqueda
 - Los motores de búsqueda nos pueden ayudar a encontrar la página web
   a la que queremos llegar o la imagen que queremos, pero también nos
   pueden servir como herramienta a través de sus operadores avanzados
   para filtrar, refinar y llegar a resultados diferentes
*** Google
 - Hay una opción de [[https://www.google.com/advanced_search][interfaz gráfica]].
 - Si realizamos varias búsquedas es posible que nos pida resolver /captcha/.
 - En [[https://github.com/flowsta/scraping#orgheadline23][este repositorio]] podéis encontrar la mayoría de operadores de búsqueda
 #+REVEAL: split:t
 - Para esta sesión vamos a repasar algunas expresiones útiles desde el propio buscador de [[https://www.google.com/][google]]:
   - El operador =site:= busca sobre un sitio. Por ejemplo, =Filomena site:elmundo.es= dará resultados sobre la búsqueda "Filomena" dentro de la página [[https://www.elmundo.es][elmundo.es]]
   - Operador =filetype= para tipo de archivo, por ejemplo, =filetype:pdf site:www.congreso.es presupuestos= busca archivos pdf sobre presupuestos en la página del congreso.
   - para una búsqueda literal utilizamos comillas dobles. por ejemplo, =site:elpais.com "ley de transparencia"=
   - el operador menos elegimos las palabras que no queremos que vengan acompañadas de otras. por ejemplo, =site:eldiario.es ley -transparencia=
*** Duckduckgo 
 - También podemos utilizar otros motores de búsqueda como [[https://duckduckgo.com/][duckduckgo]] con operadores de búsqueda similares a los de google
 - Aquí podemos repasar su [[https://help.duckduckgo.com/duckduckgo-help-pages/results/syntax/][lista de operadores]]. 
 - Existe una serie de operadores predefinidos para páginas de terceros o que funcionan como aplicaciónes. se llaman [[https://duck.co/ia][instant answers]].

* Web Scrapping con Python                                         :noexport:
** Qué es python?
 - Python es un lenguaje de programación de código abierto y multiplataforma
 - Es un lenguaje interpretado, es decir, no tenemos que compilarlo, lo cual nos ahorra mucho tiempo de desarrollo
#+REVEAL: split:t
 - Podemos instalar librerías dependiendo del propósito. Para esta sesión utilizaremos las siguientes:
   - *Request*: es una librería para descargar contenidos web como hacemos con =curl= o =wget=.
   - *Beautiful Soup* (BS) es una librería para seleccionar el HTML de la web que descargamos como hacíamos con =XPath=. Su versión mantenida es la 4
   - *Selenium*: esta librería nos permite ejecutar acciones de navegación en un navegador para extraer contenidos que no cargan en la página hasta que se produce una acción, como puede ser el /scroll/ o la navegación por un listado.
 - Con estas tres librerías podemos descargarnos todo lo que queramos de una página web.
   
** Intro a Python
- En nuestro caso trabajaremos con /python3/ que es la versión principal de desarrollo e incluye mejoras respecto a /python2/, versión que se encuentra sin soporte.
- Para iniciar =python= lo podemos hacer desde la terminal con =python= o =python3=, depende de nuestro =PATH=.
#+REVEAL: split:t
- Así entramos en la consola/terminal/intérprete de órdenes/comandos de =python=.
#+ATTR_HTML: :width 80% :align center
file:img/python3.png
- Podemos salir del intérpetre de python con la función =exit()= y enter.
#+REVEAL: split:t
- El interprete funciona de manera similar al =shell= de =unix= como hemos venido utilizándolo, lee y ejecuta comandos de forma interactiva
- Podemos revisar el historial con tecla arriba o =^P=
- Cuando se le llama con un argumento de nombre de archivo o con un archivo como entrada estándar, lee y ejecuta un script desde ese archivo:
#+BEGIN_EXAMPLE
echo 'print("¡Hola, mundo!")' > hola.py
python3 hola.py
¡Hola, mundo!
#+END_EXAMPLE
#+REVEAL: split:t
- Recomendamos un recorrido por los siguientes apartados de la documentación de Python3
  - [[https://docs.python.org/es/3.6/tutorial/introduction.html#using-python-as-a-calculator][Python como calculadora]]
  - [[https://docs.python.org/es/3.6/tutorial/introduction.html#strings][Cadena de caracteres]]
  - [[https://docs.python.org/es/3.6/tutorial/introduction.html#lists][Listas]]
- Se puede continuar la guía para profundizar más, pero es importante tener claros los conceptos que hemos listado
** Preparando python para escrapear
- Lo primero que vamos a hacer es instalar las librerías o módulos para esta práctica
- Utilizaremos un módulo llamado =pip= que viene por defecto al instalar =python3=.
- Para instalar el módulo =requests= escribimos lo siguiente en la terminal
#+BEGIN_EXAMPLE
python -m pip install requests
#+END_EXAMPLE
- En vez de =python= puede que necesites poner =python3=.
#+REVEAL: split:t
- Repetimos lo mismo para =beautifulsoup4=.
- Nótese que podemos poner más de una librería en la línea de instalación:
#+BEGIN_EXAMPLE
python -m pip install beautifulsoup4 selenium
#+END_EXAMPLE
** Bibliografía
- https://www.freecodecamp.org/learn/responsive-web-design/
- https://www.freecodecamp.org/learn/scientific-computing-with-python/
- https://www.crummy.com/software/BeautifulSoup/bs4/doc/
- https://programminghistorian.org/en/lessons/intro-to-beautiful-soup
